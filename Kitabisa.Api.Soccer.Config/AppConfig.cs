﻿using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Config
{
    public class AppConfig : IAppConfig
    {
        public string AppEnvironment => ConfigurationManager.AppSettings["App.Environment"];
        public string AppDeployment => ConfigurationManager.AppSettings["App.Deployment"];
    }
}

﻿using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kitabisa.Api.Soccer.Config.DependencyResolutions
{
    public static class DependencyConfig
    {
        public static Container ConfigureDependencies(HttpConfiguration config)
        {
            var container = new SimpleInjector.Container();

            // Select the scoped lifestyle that is appropriate for the application
            // you are building. For instance:
            //container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            container.Options.DefaultScopedLifestyle = new SimpleInjector.Lifestyles.AsyncScopedLifestyle();

            DependencyRegistry.Register(container);

            container.Verify();

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            //GlobalConst.Container = container;
            return container;
        }
    }
}

﻿using Kitabisa.Api.Soccer.Config.Injectors;
using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Service.DataImplementations;
using Kitabisa.Api.Soccer.Service.Utils;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Service.Services.Teams;
using Kitabisa.Api.Soccer.Core.Services.Players;
using Kitabisa.Api.Soccer.Service.Services.Players;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Service.Services.TeamPlayers;

namespace Kitabisa.Api.Soccer.Config.DependencyResolutions
{
    public static class DependencyRegistry
    {
        private static Container _container;
        private static IInjectorService _injectorService;

        public static void Register(Container container)
        {
            _container = container;
            _injectorService = new InjectorService(container);

            container.RegisterInstance<IInjectorService>(_injectorService);

            RegisterCore(container);
            RegisterMain(container);
        }

        private static void RegisterCore(Container container)
        {

            container.RegisterSingleton<ILoggerService, LoggerService>();
            container.Register<IAppDbContext>(() => new AppDbContext(), Lifestyle.Scoped);

            container.RegisterSingleton<IAppConfig, AppConfig>();
        }

        private static void RegisterMain(Container container)
        {
            container.Register<ITeamCommandService, TeamCommandService>(
                Lifestyle.Scoped);
            container.Register<ITeamQueryService, TeamQueryService>(
                Lifestyle.Scoped);
            container.Register<ITeamValidator, TeamValidator>(
                Lifestyle.Scoped);

            container.Register<IPlayerCommandService, PlayerCommandService>(
                Lifestyle.Scoped);
            container.Register<IPlayerQueryService, PlayerQueryService>(
                Lifestyle.Scoped);
            container.Register<IPlayerValidator, PlayerValidator>(
                Lifestyle.Scoped);

            container.Register<ITeamPlayerCommandService, TeamPlayerCommandService>(
                Lifestyle.Scoped);
            container.Register<ITeamPlayerQueryService, TeamPlayerQueryService>(
                Lifestyle.Scoped);
            container.Register<ITeamPlayerValidator, TeamPlayerValidator>(
                Lifestyle.Scoped);
        }
    }
}

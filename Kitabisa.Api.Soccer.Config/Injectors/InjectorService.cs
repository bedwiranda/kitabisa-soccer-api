﻿using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Config.Injectors
{
    public class InjectorService : IInjectorService
    {
        private readonly SimpleInjector.Container _container;

        public InjectorService(SimpleInjector.Container container)
        {
            _container = container;
        }

        public T GetInstance<T>() where T : class
        {
            return _container.GetInstance<T>();
        }
    }
}

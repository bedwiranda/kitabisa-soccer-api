﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Kitabisa.Api.Soccer.Config.DependencyResolutions;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Service.DataImplementations;
using Kitabisa.Api.Soccer.Versionings;

[assembly: OwinStartup(typeof(Kitabisa.Api.Soccer.Config.OwinStartup))]
namespace Kitabisa.Api.Soccer.Config
{
    public class OwinStartup
    {
        public OwinStartup()
        {
            //set current api version
            VersionConst.ApiLatestVersion = 4;
        }

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            HttpConfiguration config = new HttpConfiguration();

            var container = DependencyConfig.ConfigureDependencies(config);
            
            //AppStartupConfig.RegisterWebApi(config);
            
            var loggerService = container.GetInstance<ILoggerService>();

            //AppStartupConfig.ConfigureUtilsAndConsts(config, loggerService);
            using (var db = new AppDbContext())
            {
                //create database and update to latest
                db.Database.Initialize(false);
            }

            AppStartupConfig.RegisterWebApi(config);
            AppStartupConfig.RegisterApiDocs(config);
            app.UseWebApi(config);

        }
    }
}

﻿using Kitabisa.Api.Soccer.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.DTO.TeamPlayers
{
    public class TeamPlayerEntryDTO
    {
        public Guid TeamId { get;  set; }

        public Guid PlayerId { get;  set; }

        public string BackNumber { get; set; }

        public PositionType Position { get; set; }

    }
}

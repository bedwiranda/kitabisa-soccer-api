﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Entities
{
    public class Player
    {
        private string _phoneNumber;

        public Player()
            : base()
        {
        }


        [Key]
        public Guid Id { get; set; }

        public string FullName
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
            private set { }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                _phoneNumber = (!string.IsNullOrWhiteSpace(value)) ? value : null;
            }
        }
    }
}

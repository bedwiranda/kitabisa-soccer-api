﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Entities
{
    public class Team
    {
        private string _email;

        public Team()
        {

        }

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email
        {
            get { return _email; }
            set { _email = (!string.IsNullOrWhiteSpace(value)) ? value.Trim().ToLower() : null; }
        }

        // navigations
        public ICollection<TeamPlayer> Players { get; set; }
    }
}

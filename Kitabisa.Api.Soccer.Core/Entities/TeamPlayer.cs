﻿using Kitabisa.Api.Soccer.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Entities
{
    public class TeamPlayer
    {
        public TeamPlayer()
            : base()
        {
        }

        public TeamPlayer(Guid teamId, Guid playerId)
            : this()
        {
            TeamId = teamId;
            PlayerId = playerId;
        }


        [Key]
        [Column(Order = 1)]
        public Guid TeamId { get; protected set; }


        [Key]
        [Column(Order = 2)]
        public Guid PlayerId { get; protected set; }

        public string BackNumber { get; set; }

        public PositionType Position { get; set; }

        //navigations
        // public Team Team { get; set; }
        public Player Player { get; set; }
    }
}

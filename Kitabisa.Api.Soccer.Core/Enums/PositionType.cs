﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Enums
{
    public enum PositionType
    {
        GoalKeeper,
        LeftBack,
        RightBack,
        CentreBack,
        LeftMidFielder,
        RightMidFielder,
        CentreMidFielder,
        RightWing,
        LeftWing,
        Striker
    }
}

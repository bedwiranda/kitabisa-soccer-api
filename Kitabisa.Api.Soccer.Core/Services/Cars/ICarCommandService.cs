﻿using Kitabisa.Api.Soccer.Core.DTO.Soccer;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Repository.Soccer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Soccer
{
    public interface ICarCommandService
    {
        Task<ServiceResult<Car>> AddCarAsync(CarCreateDTO dto);

        Task<ServiceResult<Car>> UpdateCarAsync(int id, CarUpdateDTO dto);
    }
}

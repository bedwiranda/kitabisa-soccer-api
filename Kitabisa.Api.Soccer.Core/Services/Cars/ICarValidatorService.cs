﻿using Kitabisa.Api.Soccer.Core.DTO.Soccer;
using Kitabisa.Repository.Soccer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Soccer
{
    public interface ICarValidatorService
    {
        Task<bool> ValidateCarCreateAsync(ICarRepository repo, CarCreateDTO dto);

        Task<bool> ValidateCarUpdateAsync(ICarRepository repo, Car car, CarUpdateDTO dto);
    }
}

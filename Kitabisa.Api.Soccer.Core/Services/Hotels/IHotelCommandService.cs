﻿using Kitabisa.Api.Soccer.Core.DTO.Soccer;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Cores.Services.Sales
{
    public interface IHotelCommandService
    {
        Task<ServiceResult<Hotel>> AddHotelAsync(HotelCreateDTO dto);
        Task<ServiceResult<Hotel>> UpdateHotelAsync(Guid hotelId, HotelUpdateDTO dto);
    }
}

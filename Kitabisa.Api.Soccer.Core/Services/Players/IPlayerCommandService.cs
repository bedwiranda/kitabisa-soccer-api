﻿using Kitabisa.Api.Soccer.Core.DTO.Players;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Players
{
    public interface IPlayerCommandService
    {
        Task<ServiceResult<Player>> CreatePlayerAsync(PlayerCreateDTO dto);

        Task<ServiceResult<Player>> UpdatePlayerAsync(Guid playerId, PlayerUpdateDTO dto);
    }
}

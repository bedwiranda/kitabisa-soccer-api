﻿using Kitabisa.Api.Soccer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Players
{
    public interface IPlayerQueryService
    {
        Task<ICollection<Player>> GetPlayerAsync();
        Task<Player> GetPlayerByNameAsync(string name);
    }
}

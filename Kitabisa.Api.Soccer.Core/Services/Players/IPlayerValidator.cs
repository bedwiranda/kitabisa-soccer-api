﻿using Kitabisa.Api.Soccer.Core.DTO.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Players
{
    public interface IPlayerValidator
    {
        Task<bool> ValidatePlayerCreateAsync(PlayerCreateDTO dto);
        Task<bool> ValidatePlayerUpdateAsync(PlayerUpdateDTO dto);
    }
}

﻿using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.TeamPlayers
{
    public interface ITeamPlayerCommandService
    {
        Task<ServiceResult<TeamPlayer>> CreateTeamPlayerAsync(TeamPlayerCreateDTO dto);

        Task<ServiceResult<TeamPlayer>> UpdateTeamPlayerAsync(TeamPlayerUpdateDTO dto);
    }
}

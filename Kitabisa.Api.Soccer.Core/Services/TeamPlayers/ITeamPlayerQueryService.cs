﻿using Kitabisa.Api.Soccer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.TeamPlayers
{
    public interface ITeamPlayerQueryService
    {
        Task<ICollection<Player>> GetTeamPlayerAsync(Guid teamId);
        Task<Player> GetTeamPlayerByNameAsync(Guid teamId, string name);

    }
}

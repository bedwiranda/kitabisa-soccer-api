﻿using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.TeamPlayers
{
    public interface ITeamPlayerValidator
    {
        Task<bool> ValidateTeamPlayerCreateAsync(TeamPlayerCreateDTO dto);
        Task<bool> ValidateTeamPlayerUpdateAsync(TeamPlayerUpdateDTO dto);

    }
}

﻿using Kitabisa.Api.Soccer.Core.DTO.Teams;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Shares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Teams
{
    public interface ITeamCommandService
    {
        Task<ServiceResult<Team>> CreateTeamAsync(TeamCreateDTO dto);

        Task<ServiceResult<Team>> UpdateTeamAsync(Guid teamId, TeamUpdateDTO dto);

    }
}

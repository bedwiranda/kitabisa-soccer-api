﻿using Kitabisa.Api.Soccer.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Teams
{
    public interface ITeamQueryService
    {
        Task<ICollection<Team>> GetTeamAsync();
        Task<Team> GetTeamByNameAsync(string name);

    }
}

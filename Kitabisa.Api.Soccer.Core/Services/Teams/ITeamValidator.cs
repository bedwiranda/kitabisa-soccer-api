﻿using Kitabisa.Api.Soccer.Core.DTO.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Services.Teams
{
    public interface ITeamValidator
    {
        Task<bool> ValidateTeamCreateAsync(TeamCreateDTO dto);
        Task<bool> ValidateTeamUpdateAsync(TeamUpdateDTO dto);

    }
}

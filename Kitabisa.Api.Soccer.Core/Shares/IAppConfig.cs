﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Shares
{
    public interface IAppConfig
    {
        string AppEnvironment { get; }
        string AppDeployment { get; }
    }
}

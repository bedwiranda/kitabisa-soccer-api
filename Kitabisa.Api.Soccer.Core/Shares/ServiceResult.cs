﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Core.Shares
{
    public class ServiceResult<T>
    {
        public ServiceResult()
        {
            Succeded = false;
        }

        public ServiceResult(bool succeded, T data)
        {
            Succeded = succeded;
            Data = data;
        }

        public bool Succeded { get; private set; }
        public T Data { get; private set; }
    }
}

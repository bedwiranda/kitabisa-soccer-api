﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.Entity;
using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Entities;

namespace Kitabisa.Api.Soccer.Service.DataImplementations
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext()
            : base("Name=KitabisaContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            this.Database.CommandTimeout = 120;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().ToTable("Team");
            modelBuilder.Entity<Player>().ToTable("Player");
            modelBuilder.Entity<TeamPlayer>().ToTable("TeamPlayer");
        }
    }
}

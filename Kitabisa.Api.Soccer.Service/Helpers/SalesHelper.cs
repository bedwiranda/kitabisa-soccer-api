﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Helpers
{
    public class SalesHelper
    {
        public static decimal CalculateDiscountBefore2000(decimal sumPrice, int carYear, decimal carPrice)
        {
            var discountLimitYear = 2000;

            if (carYear < discountLimitYear)
            {
                sumPrice = sumPrice + carPrice - (carPrice * 10 / 100);
            }
            else
            {
                sumPrice = sumPrice + carPrice;
            }

            return sumPrice;
        }

        public static decimal CalculateDiscountBasedOnNumberOfSoccer(decimal sumPrice, ICollection<int> carIds)
        {
            var numOfSoccerToBuy = carIds.Count();
            if (numOfSoccerToBuy > 2)
            {
                sumPrice = sumPrice - (sumPrice * 3 / 100);
            }

            return sumPrice;
        }

        public static decimal CalculateDiscountBasedOnTotalCost(decimal sumPrice)
        {
            var limitDiscount5 = 100000;
            if (sumPrice > limitDiscount5)
            {
                sumPrice = sumPrice - (sumPrice * 5 / 100);
            }
            return sumPrice;
        }
    }
}

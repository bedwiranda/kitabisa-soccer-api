﻿using Kitabisa.Api.Soccer.Core.DTO.Soccer;
using Kitabisa.Api.Soccer.Core.Services.Soccer;
using Kitabisa.Repository.Soccer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Soccer
{
    public class CarValidatorService : ICarValidatorService
    {
        public async Task<bool> ValidateCarCreateAsync(ICarRepository repo, CarCreateDTO dto)
        {
            //we can add validation here
            if (dto.Price < 0) return false;

            return true;
        }

        public async Task<bool> ValidateCarUpdateAsync(ICarRepository repo, Car car, CarUpdateDTO dto)
        {
            //we can add validation here
            if (dto.Price < 0) return false;

            return true;
        }
    }
}

﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.DTO.Soccer;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Cores.Services.Sales;
using Kitabisa.Api.Soccer.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Soccer
{
    public class HotelCommandService : IHotelCommandService
    {
        public IAppDbContext _context;
        public IHotelValidatorService _salesValidator;
        private readonly ILoggerService _loggerService;

        public HotelCommandService(IAppDbContext context, IHotelValidatorService salesValidator, ILoggerService loggerService)
            : base()
        {
            _context = context;
            _salesValidator = salesValidator;
            _loggerService = loggerService;
        }

        public async Task<ServiceResult<Hotel>> AddHotelAsync(HotelCreateDTO dto)
        {
            var failResult = new ServiceResult<Hotel>();

            var hotel = new Hotel()
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                Address = dto.Address,
                Phone = dto.Phone,
                JoinDateUtc = DateTime.UtcNow
            };

            try
            {
                _context.Set<Hotel>().Add(hotel);
                await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {

            }

            return new ServiceResult<Hotel>(true, hotel);
        }

        public async Task<ServiceResult<Hotel>> UpdateHotelAsync(Guid hotelId, HotelUpdateDTO dto)
        {
            var failResult = new ServiceResult<Hotel>();

            var hotel = new Hotel()
            {

            };

            return new ServiceResult<Hotel>(true, hotel);
        }
    }
}

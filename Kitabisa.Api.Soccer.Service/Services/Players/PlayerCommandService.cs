﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.DTO.Players;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.Players;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Players
{
    public class PlayerCommandService : IPlayerCommandService
    {
        public IAppDbContext _context;
        public IPlayerValidator _validator;
        private readonly ILoggerService _loggerService;
        private readonly IAppConfig _appConfig;

        public PlayerCommandService(IAppDbContext context, IPlayerValidator validator, ILoggerService loggerService, IAppConfig appConfig)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
            _appConfig = appConfig;
        }

        public async Task<ServiceResult<Player>> CreatePlayerAsync(PlayerCreateDTO dto)
        {
            var failResult = new ServiceResult<Player>();

            if (!await _validator.ValidatePlayerCreateAsync(dto)) return failResult;

            var data = new Player()
            {
                Id = Guid.NewGuid(),
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PhoneNumber = dto.PhoneNumber,
                Email = dto.Email
            };

            try
            {
                _context.Set<Player>().Add(data);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<Player>(true, data);
        }

        public async Task<ServiceResult<Player>> UpdatePlayerAsync(Guid playerId, PlayerUpdateDTO dto)
        {
            var failResult = new ServiceResult<Player>();

            var data = _context.Set<Player>().Where(x => x.Id == playerId).FirstOrDefault();
            data.FirstName = dto.FirstName;
            data.LastName = dto.LastName;
            data.PhoneNumber = dto.PhoneNumber;
            data.Email = dto.Email;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<Player>(true, data);
        }
    }
}

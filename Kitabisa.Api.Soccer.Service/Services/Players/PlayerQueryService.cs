﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.Players;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Players
{
    public class PlayerQueryService : IPlayerQueryService
    {
        public IAppDbContext _context;
        public IPlayerValidator _validator;
        private readonly ILoggerService _loggerService;

        public PlayerQueryService(IAppDbContext context, IPlayerValidator validator, ILoggerService loggerService)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
        }

        public async Task<ICollection<Player>> GetPlayerAsync()
        {
            var data = await _context.Set<Player>().ToListAsync();

            return data;
        }

        public async Task<Player> GetPlayerByNameAsync(string name)
        {
            var data = await _context.Set<Player>().Where(x => x.FirstName.Contains(name) || x.LastName.Contains(name)).FirstOrDefaultAsync();

            return data;
        }
    }
}

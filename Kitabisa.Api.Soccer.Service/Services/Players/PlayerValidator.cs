﻿using Kitabisa.Api.Soccer.Core.DTO.Players;
using Kitabisa.Api.Soccer.Core.Services.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Players
{
    public class PlayerValidator : IPlayerValidator
    {
        public async Task<bool> ValidatePlayerCreateAsync(PlayerCreateDTO dto)
        {
            // insert validation logic here
            return true;
        }

        public async Task<bool> ValidatePlayerUpdateAsync(PlayerUpdateDTO dto)
        {
            // insert validation logic here
            return true;
        }
    }
}

﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.TeamPlayers
{
    public class TeamPlayerCommandService : ITeamPlayerCommandService
    {
        public IAppDbContext _context;
        public ITeamPlayerValidator _validator;
        private readonly ILoggerService _loggerService;
        private readonly IAppConfig _appConfig;

        public TeamPlayerCommandService(IAppDbContext context, ITeamPlayerValidator validator, ILoggerService loggerService, IAppConfig appConfig)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
            _appConfig = appConfig;
        }

        public async Task<ServiceResult<TeamPlayer>> CreateTeamPlayerAsync(TeamPlayerCreateDTO dto)
        {
            var failResult = new ServiceResult<TeamPlayer>();

            if (!await _validator.ValidateTeamPlayerCreateAsync(dto)) return failResult;

            var data = new TeamPlayer(dto.TeamId, dto.PlayerId)
            {
                BackNumber = dto.BackNumber,
                Position = dto.Position
            };

            try
            {
                _context.Set<TeamPlayer>().Add(data);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<TeamPlayer>(true, data);
        }

        public async Task<ServiceResult<TeamPlayer>> UpdateTeamPlayerAsync(TeamPlayerUpdateDTO dto)
        {
            var failResult = new ServiceResult<TeamPlayer>();

            var data = await _context.Set<TeamPlayer>().Where(x => x.TeamId == dto.TeamId && x.PlayerId == dto.PlayerId).FirstOrDefaultAsync();
            data.BackNumber = dto.BackNumber;
            data.Position = dto.Position;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<TeamPlayer>(true, data);
        }
    }
}

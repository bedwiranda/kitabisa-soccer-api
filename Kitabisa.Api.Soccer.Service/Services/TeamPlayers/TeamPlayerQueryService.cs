﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.TeamPlayers
{
    public class TeamPlayerQueryService : ITeamPlayerQueryService
    {
        public IAppDbContext _context;
        public ITeamPlayerValidator _validator;
        private readonly ILoggerService _loggerService;

        public TeamPlayerQueryService(IAppDbContext context, ITeamPlayerValidator validator, ILoggerService loggerService)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
        }

        public async Task<ICollection<Player>> GetTeamPlayerAsync(Guid teamId)
        {
            var data = await _context.Set<TeamPlayer>().Include(x => x.Player).Where(x => x.TeamId == teamId)
                .Select(x => x.Player).ToListAsync();

            return data;
        }

        public async Task<Player> GetTeamPlayerByNameAsync(Guid teamId, string name)
        {
            var data = await _context.Set<TeamPlayer>().Where(x => x.TeamId == teamId && (x.Player.FirstName.Contains(name)
            || x.Player.LastName.Contains(name))).Select(x => x.Player).FirstOrDefaultAsync();

            return data;
        }
    }
}

﻿using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.TeamPlayers
{
    public class TeamPlayerValidator : ITeamPlayerValidator
    {
        public async Task<bool> ValidateTeamPlayerCreateAsync(TeamPlayerCreateDTO dto)
        {
            // insert validation logic here
            return true;
        }

        public async Task<bool> ValidateTeamPlayerUpdateAsync(TeamPlayerUpdateDTO dto)
        {
            // insert validation logic here
            return true;
        }
    }
}

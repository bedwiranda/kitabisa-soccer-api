﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.DTO.Teams;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Core.Shares;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Teams
{
    public class TeamCommandService : ITeamCommandService
    {
        public IAppDbContext _context;
        public ITeamValidator _validator;
        private readonly ILoggerService _loggerService;
        private readonly IAppConfig _appConfig;

        public TeamCommandService(IAppDbContext context, ITeamValidator validator, ILoggerService loggerService, IAppConfig appConfig)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
            _appConfig = appConfig;
        }

        public async Task<ServiceResult<Team>> CreateTeamAsync(TeamCreateDTO dto)
        {
            var failResult = new ServiceResult<Team>();

            if (!await _validator.ValidateTeamCreateAsync(dto)) return failResult;

            var data = new Team()
            {
                Id = Guid.NewGuid(),
                Name = dto.Name,
                Address = dto.Address,
                Email = dto.Email,
                Phone = dto.Phone
            };

            try
            {
                _context.Set<Team>().Add(data);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<Team>(true, data);
        }

        public async Task<ServiceResult<Team>> UpdateTeamAsync(Guid teamId, TeamUpdateDTO dto)
        {
            var failResult = new ServiceResult<Team>();

            var data = _context.Set<Team>().Where(x => x.Id == teamId).FirstOrDefault();
            data.Name = dto.Name;
            data.Address = dto.Address;
            data.Email = dto.Email;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return new ServiceResult<Team>(true, data);
        }
    }
}

﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Entities;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Teams
{
    public class TeamQueryService : ITeamQueryService
    {
        public IAppDbContext _context;
        public ITeamValidator _validator;
        private readonly ILoggerService _loggerService;

        public TeamQueryService(IAppDbContext context, ITeamValidator validator, ILoggerService loggerService)
            : base()
        {
            _context = context;
            _validator = validator;
            _loggerService = loggerService;
        }

        public async Task<ICollection<Team>> GetTeamAsync()
        {
            var data = await _context.Set<Team>().ToListAsync();

            return data;
        }

        public async Task<Team> GetTeamByNameAsync(string name)
        {
            var data = await _context.Set<Team>().Where(x => x.Name.Contains(name))
                .Include(x => x.Players.Select(x2 => x2.Player))
                .FirstOrDefaultAsync();

            return data;
        }
    }
}

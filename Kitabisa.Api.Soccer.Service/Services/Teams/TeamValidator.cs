﻿using Kitabisa.Api.Soccer.Core.DTO.Teams;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Services.Teams
{
    public class TeamValidator : ITeamValidator
    {
        public async Task<bool> ValidateTeamCreateAsync(TeamCreateDTO dto)
        {
            return true;
        }

        public async Task<bool> ValidateTeamUpdateAsync(TeamUpdateDTO dto)
        {
            return true;
        }
    }
}

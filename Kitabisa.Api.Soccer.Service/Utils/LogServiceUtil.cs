﻿using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Service.Utils
{
    public class LoggerService : ILoggerService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LoggerService()
        {
        }

        private string ConstructMessage(string msg, string category = null, string subcategory = null)
        {
            if (category == null && subcategory == null) return msg;

            var strCat = (subcategory != null) ? $"{category} | {subcategory}".ToUpper() : $"{category}".ToUpper();
            return $"{strCat} -> {msg}";
        }

        public void LogInfo(string msg, string category = null, string subcategory = null)
        {
            try
            {
                Log.Info(ConstructMessage(msg, category: category, subcategory: subcategory));
            }
            catch (Exception e)
            {
                //dont catch any error
            }
        }

        public void LogWarning(string msg, string category = null, string subcategory = null, Exception exception = null)
        {
            try
            {
                if (exception != null)
                {
                    Log.Warn(ConstructMessage(msg, category: category, subcategory: subcategory), exception);
                }
                else
                {
                    Log.Warn(ConstructMessage(msg, category: category, subcategory: subcategory));
                }
            }
            catch (Exception e)
            {
                //dont catch any error
            }
        }

        public void LogError(Exception exception, string msg, string category = null, string subcategory = null)
        {
            try
            {
                Log.Error(ConstructMessage(msg, category: category, subcategory: subcategory), exception);
            }
            catch (Exception e)
            {
                //dont catch any error
            }
        }
        
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Tests.Problem1
{
    public class AnswerService
    {
        public string CalculateFromString(string data)
        {
            // we want the sign for fisrt data
            // eg "5 + 6 + 10" should be "+5 + 6 + 10"
            // if "-5 + 6" its okay

            var failResult = "validation error";

            if (data[0] != '-' && data[0] != '+') data = '+' + data;

            var length = data.Length;

            // we dont want operator at the end of string
            if (data[length - 1] == '+' || data[length - 1] == '-') return failResult;

            var lastSignIndex = length;
            var result = 0;
            try
            {
                for (var i = length - 1; i >= 0; i--)
                {
                    if (data[i] == '+')
                    {
                        // Here i use substring to get value like "5 + 565251" becoming " 565251", and parse it to integer
                        // The trim is to clear the whitespacing like " 565251" would be "565251"
                        result = result + int.Parse(data.Substring(i + 1, lastSignIndex - i - 1).Trim());
                        lastSignIndex = i;
                    }
                    else if (data[i] == '-')
                    {
                        result = result - int.Parse(data.Substring(i + 1, lastSignIndex - i - 1).Trim());
                        lastSignIndex = i;
                    }
                }
            }
            catch
            {
                return failResult;
            }

            return result.ToString();
        }
    }
}

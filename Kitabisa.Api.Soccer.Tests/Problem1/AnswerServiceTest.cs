﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitabisa.Api.Soccer.Tests.Problem1
{
    [TestFixture]
    public class AnswerServiceTest
    {
        [Test, TestCaseSource("DataSamples")]
        public async Task CalculateFromString_GivenData_ReturnResult(string input, string expectedOutput)
        {
            var service = new AnswerService();
            var result = service.CalculateFromString(input);

            // Assert
            Assert.AreEqual(expectedOutput, result);
        }

        static object[][] DataSamples = new object[][]
        {
            new object[] { "1+1+1", "3" },
            new object[] { "1 + 5 + 6", "12" },
            new object[] { "-5 + 6 + 7", "8" },
            new object[] { "  100   + 500+600", "1200" },
            new object[] { "1+abcedef+5", "validation error" },
            new object[] { "1  + abc def + 5", "validation error" },
            new object[] { "1 + 5++", "validation error" },
            new object[] { "abcdefo", "validation error" },
        };

    }
}

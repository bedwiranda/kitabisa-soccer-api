﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Service.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kitabisa.Api.Soccer.Core.Services.Players;
using Kitabisa.Api.Soccer.Service.Services.Players;
using Kitabisa.Api.Soccer.Core.DTO.Players;
using Moq;
using Kitabisa.Api.Soccer.Core.Entities;
using NSubstitute;
using System.Data.Entity;
using Kitabisa.Api.Soccer.Core.Shares;
using System.Data.Entity.Infrastructure;

namespace Kitabisa.Api.Soccer.Tests.Services
{
    [TestFixture]
    public class PlayerUnitTest
    {
        //initializer
        private static IPlayerValidator _validator;
        private static IPlayerCommandService _commandService;
        private static ILoggerService _loggerService;
        private static IAppDbContext _context;
        private static IAppConfig _appConfig;

        [OneTimeSetUp]
        public void setUp()
        {
            _validator = new PlayerValidator();
            _loggerService = new LoggerService();
            _context = Substitute.For<IAppDbContext>();
            _appConfig = Substitute.For<IAppConfig>();
        }

        #region HELPERS

        private static PlayerCommandService CreatePlayerCommandService()
        {

            var command = new PlayerCommandService(_context, _validator, _loggerService, _appConfig);

            return command;
        }

        #endregion HELPERS

        [Test]
        public async Task CreatePlayerAsync_GivenValidData_ReturnServiceResult()
        {
            //arrange
            var data = new PlayerCreateDTO()
            {
                FirstName = "David",
                LastName = "Beckam",
                PhoneNumber = "082323432423"
            };

            _context.Set<Player>().Returns(x => Substitute.For<DbSet<Player>>());
            _context.Set<Player>().Add(Arg.Any<Player>()).Returns(x => null);
            _context.SaveChangesAsync().Returns(x => Task.FromResult(1));

            //create command service
            var commandService = CreatePlayerCommandService();

            //action
            var serviceResult = await commandService.CreatePlayerAsync(data);

            //assert
            Assert.That(serviceResult.Succeded, Is.True);
        }

        [Test]
        public async Task UpdatePlayerAsync_GivenValidData_ReturnServiceResult()
        {
            //arrange
            var playerId1 = new Guid("97a9c0a6-ef3f-44b0-af05-eee973011b24");
            var playerId2 = new Guid("22c728e0-b872-46db-aba4-5e398a215ce2");

            var data = new PlayerUpdateDTO()
            {
                FirstName = "David",
                LastName = "Beckam",
                PhoneNumber = "082323432423"
            };

            var players = GetFakeListOfPlayers(playerId1, playerId2);
            var entities = GetQueryableMockDbSet<Player>(players);
            _context.Set<Player>().Returns(x => entities);
           // _context.Set<Player>().Add(Arg.Any<Player>()).Returns(x => null);
            _context.SaveChangesAsync().Returns(x => Task.FromResult(1));

            //create command service
            var commandService = CreatePlayerCommandService();

            //action
            var serviceResult = await commandService.UpdatePlayerAsync(playerId1, data);

            //assert
            Assert.That(serviceResult.Succeded, Is.True);
        }


        private Player[] GetFakeListOfPlayers(Guid playerId1, Guid playerId2)
        {
            var players = new Player[]
            {
                new Player {Id = playerId1, FirstName = "David", LastName = "Beckham"},
                new Player {Id = playerId2, FirstName = "Wayne", LastName = "Rooney"}
            };

            return players;
        }

        private static DbSet<T> GetQueryableMockDbSet<T>(params T[] sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return dbSet.Object;
        }
    }
    
}

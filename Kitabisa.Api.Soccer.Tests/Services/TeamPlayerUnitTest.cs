﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Service.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Service.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Entities;
using NSubstitute;
using System.Data.Entity;
using Kitabisa.Api.Soccer.Core.Shares;

namespace Kitabisa.Api.Soccer.Tests.Services
{
    [TestFixture]
    public class TeamPlayerUnitTest
    {
        //initializer
        private static ITeamPlayerValidator _validator;
        private static ITeamPlayerCommandService _commandService;
        private static ILoggerService _loggerService;
        private static IAppDbContext _context;
        private static IAppConfig _appConfig;

        [OneTimeSetUp]
        public void setUp()
        {
            _validator = new TeamPlayerValidator();
            _loggerService = new LoggerService();
            _context = Substitute.For<IAppDbContext>();
            _appConfig = Substitute.For<IAppConfig>();
        }

        #region HELPERS

        private static TeamPlayerCommandService CreateTeamPlayerCommandService()
        {

            var commandService = new TeamPlayerCommandService( _context, _validator, _loggerService, _appConfig);

            return commandService;
        }

        #endregion HELPERS

        [Test]
        public async Task CreateTeamPlayerAsync_GivenValidData_ReturnServiceResult()
        {
            //arrange
            var data = new TeamPlayerCreateDTO()
            {
                TeamId = Guid.NewGuid(),
                PlayerId = Guid.NewGuid(),
                BackNumber = "12",
                Position = Core.Enums.PositionType.GoalKeeper
            };

            _context.Set<TeamPlayer>().Returns(x => Substitute.For<DbSet<TeamPlayer>>());
            _context.Set<TeamPlayer>().Add(Arg.Any<TeamPlayer>()).Returns(x => null);
            _context.SaveChangesAsync().Returns(x => Task.FromResult(1));

            //create command service
            var commandService = CreateTeamPlayerCommandService();

            //action
            var serviceResult = await commandService.CreateTeamPlayerAsync(data);

            //assert
            Assert.That(serviceResult.Succeded, Is.True);
        }
    }
    
}

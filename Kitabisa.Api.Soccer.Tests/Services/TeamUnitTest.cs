﻿using Kitabisa.Api.Soccer.Core.DataInterfaces;
using Kitabisa.Api.Soccer.Core.Utils;
using Kitabisa.Api.Soccer.Service.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Service.Services.Teams;
using Kitabisa.Api.Soccer.Core.DTO.Teams;
using NSubstitute;
using Kitabisa.Api.Soccer.Core.Entities;
using System.Data.Entity;
using Kitabisa.Api.Soccer.Core.Shares;
using Moq;

namespace Kitabisa.Api.Soccer.Tests.Services
{
    [TestFixture]
    public class TeamUnitTest
    {
        //initializer
        private static ITeamValidator _validator;
        private static ITeamCommandService _commandService;
        private static ILoggerService _loggerService;
        private static IAppDbContext _context;
        private static IAppConfig _appConfig;

        [OneTimeSetUp]
        public void setUp()
        {
            _validator = new TeamValidator();
            _loggerService = new LoggerService();
            _context = Substitute.For<IAppDbContext>();
            _appConfig = Substitute.For<IAppConfig>();
        }

        #region HELPERS

        private static TeamCommandService CreateTeamCommandService()
        {

            var command = new TeamCommandService( _context, _validator, _loggerService, _appConfig);

            return command;
        }

        #endregion HELPERS

        [Test]
        public async Task CreateTeamAsync_GivenValidData_ReturnServiceResult()
        {
            //arrange
            var data = new TeamCreateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };

            _context.Set<Team>().Returns(x => Substitute.For<DbSet<Team>>());
            _context.Set<Team>().Add(Arg.Any<Team>()).Returns(x => null);
            _context.SaveChangesAsync().Returns(x => Task.FromResult(1));

            //create command service
            var commandService = CreateTeamCommandService();

            //action
            var serviceResult = await commandService.CreateTeamAsync(data);

            //assert

            Assert.That(serviceResult.Succeded, Is.True);
        }

        [Test]
        public async Task UpdateTeamAsync_GivenValidData_ReturnServiceResult()
        {
            //arrange
            var teamId1 = new Guid("97a9c0a6-ef3f-44b0-af05-eee973011b24");
            var teamId2 = new Guid("22c728e0-b872-46db-aba4-5e398a215ce2");

            var data = new TeamUpdateDTO()
            {
                Name = "Hotel1",
                Address = "Bandung",
                Phone = "082323432423"
            };

            var teams = GetFakeList(teamId1, teamId2);
            var entities = GetQueryableMockDbSet<Team>(teams);
            _context.Set<Team>().Returns(x => entities);
            _context.SaveChangesAsync().Returns(x => Task.FromResult(1));

            //create command service
            var commandService = CreateTeamCommandService();

            //action
            var serviceResult = await commandService.UpdateTeamAsync(teamId1, data);

            //assert

            Assert.That(serviceResult.Succeded, Is.True);
        }
        private Team[] GetFakeList(Guid teamId1, Guid teamId2)
        {
            var teams = new Team[]
            {
                new Team {Id = teamId1, Name = "Barcelona", Phone = "08234324234"},
                new Team {Id = teamId2, Name = "Manchester United", Phone = "083423432"}
            };

            return teams;
        }

        private static DbSet<T> GetQueryableMockDbSet<T>(params T[] sourceList) where T : class
        {
            var queryable = sourceList.AsQueryable();

            var dbSet = new Mock<DbSet<T>>();
            dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

            return dbSet.Object;
        }
    }
    
}

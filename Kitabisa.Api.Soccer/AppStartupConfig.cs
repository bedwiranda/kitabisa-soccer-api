﻿using Kitabisa.Api.Soccer.Configs;
using Kitabisa.Api.Soccer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Kitabisa.Api.Soccer
{
    public class AppStartupConfig
    {
        public static void RegisterWebApi(HttpConfiguration config)
        {
            WebApiConfig.Register(config);
        }

        public static void ConfigureUtilsAndConsts(HttpConfiguration config,
            ILoggerService loggerService)
        {
            LoggerConfig.Configure(loggerService);
        }

        public static void RegisterApiDocs(HttpConfiguration config)
        {
            SwashbuckleConfig.Register(config, "Kitabisa.Api.Soccer", new List<SwashbuckleVersionDefinition>()
            {
                new SwashbuckleVersionDefinition("1", "Kitabisa API V1"),
                new SwashbuckleVersionDefinition("2", "Kitabisa API V2"),
                new SwashbuckleVersionDefinition("3", "Kitabisa API V3"),
                new SwashbuckleVersionDefinition("4", "Kitabisa API V4")
            });
        }
    }
}
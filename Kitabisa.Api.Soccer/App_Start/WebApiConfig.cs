﻿using Kitabisa.Api.Soccer.Versionings;
using System.Web.Http;

namespace Kitabisa.Api.Soccer
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //versioning. Manipulate request url for versioning
            //http://stackoverflow.com/questions/14365953/in-net-mvc-4-web-api-how-do-i-intercept-a-request-for-a-controller-and-chang
            config.MessageHandlers.Add(new VersionUrlHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "resources/v{version}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

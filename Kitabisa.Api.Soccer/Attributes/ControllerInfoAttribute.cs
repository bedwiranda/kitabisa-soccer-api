﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kitabisa.Api.Soccer.Attributes
{
    /// <summary>
    /// Attribute to add more information to controller
    /// If put in method, then it should by pass the info in controller class
    /// Used by swashbuckle
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ControllerInfoAttribute : Attribute
    {
        private readonly string _categoryValue;
        private readonly string _titleValue;
        private readonly string _descriptionValue;

        public ControllerInfoAttribute(string category, string title, string description = null)
        {
            _categoryValue = category;
            _titleValue = title;
            _descriptionValue = description;
        }

        public string Category { get { return _categoryValue; } }
        public string Title { get { return _titleValue; } }
        public string Description { get { return _descriptionValue; } }
    }
}
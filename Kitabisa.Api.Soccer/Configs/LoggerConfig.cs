﻿using Kitabisa.Api.Soccer.Core.Utils;
using log4net;
using log4net.Appender;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kitabisa.Api.Soccer.Configs
{
    public static class LoggerConfig
    {
        public static ILoggerService LoggerService { get; private set; }

        public static void Configure( ILoggerService loggerService)
        {
            SetThreshold("FileAppender", Level.All);

            LoggerService = loggerService;
        }

        private static void SetThreshold(string appenderName, Level threshold)
        {
            foreach (AppenderSkeleton appender in LogManager.GetRepository().GetAppenders())
            {
                if (appender.Name == appenderName)
                {
                    appender.Threshold = threshold;
                    appender.ActivateOptions();
                    break;
                }
            }
        }
    }
}
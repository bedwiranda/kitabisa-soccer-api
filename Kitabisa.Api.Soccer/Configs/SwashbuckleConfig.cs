﻿using Kitabisa.Api.Soccer.Attributes;
using Kitabisa.Api.Soccer.Versionings;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Kitabisa.Api.Soccer.Configs
{
    public class SwashbuckleConfig
    {
        public static void Register(HttpConfiguration config, string projectName,
            ICollection<SwashbuckleVersionDefinition> versionDefs, bool isAuth = false)
        {
            config
                .EnableSwagger("docs-resources/{apiVersion}", c =>
                {
                    c.MultipleApiVersions(
                        (apiDesc, targetApiVersion) => ResolveVersionSupportByRouteConstraint(apiDesc, targetApiVersion),
                        vc =>
                        {
                            foreach (var vdef in versionDefs)
                            {
                                vc.Version(vdef.Version, vdef.Title);
                            }
                        });

                    c.GroupActionsBy(GetControllerGroup);
                    c.IncludeXmlComments(GetXmlCommentsPath(projectName));

                    //ref: https://github.com/domaindrivendev/Swashbuckle/issues/442
                    c.UseFullTypeNameInSchemaIds();

                    if (isAuth)
                    {
                        c.DocumentFilter<AuthTokenOperation>();
                    }
                })
                .EnableSwaggerUi("docs/{*assetPath}", c => { c.EnableDiscoveryUrlSelector(); });
        }

        private static string GetControllerGroup(System.Web.Http.Description.ApiDescription arg)
        {
            var arr = arg.ActionDescriptor.ControllerDescriptor.ControllerType.FullName.Split('.');
            if (arr.Length > 0)
            {
                var controllerFullName = arr.LastOrDefault();
                if (controllerFullName != null)
                {
                    var ctrlInfo =
                        arg.ActionDescriptor.GetCustomAttributes<ControllerInfoAttribute>().FirstOrDefault() ??
                        arg.ActionDescriptor.ControllerDescriptor.GetCustomAttributes<ControllerInfoAttribute>()
                            .FirstOrDefault();
                    if (ctrlInfo != null)
                    {
                        return string.Format("{0} - {1}", ctrlInfo.Category, ctrlInfo.Title);
                    }

                    var controllerName = Regex.Match(controllerFullName, @"[a-zA-Z0-9]+(?=V\d+Controller)").Value;
                    return $"General - {controllerName}";
                }
            }

            return "General";
        }

        private static string GetXmlCommentsPath(string projectName)
        {
            var path = $@"{System.AppDomain.CurrentDomain.BaseDirectory}bin\{projectName}.XML";
            return path;
        }

        private static bool ResolveVersionSupportByRouteConstraint(System.Web.Http.Description.ApiDescription apiDesc,
            string version)
        {
            var arr = apiDesc.ActionDescriptor.ControllerDescriptor.ControllerType.FullName.Split('.');
            var controllerFullName = arr.LastOrDefault();

            var ver = int.Parse(version);
            var verInfo = apiDesc.ActionDescriptor.GetCustomAttributes<VersionedRouteAttribute>().FirstOrDefault();
            if (verInfo != null)
            {
                if (controllerFullName == "TestController")
                {
                    var x = 1;
                    var y = x;
                }

                if (verInfo.MinimumVersion != null)
                {
                    return ver >= verInfo.MinimumVersion.Value && ver <= verInfo.AllowedVersion;
                }
                else
                {
                    return ver <= verInfo.AllowedVersion;
                }
            }

            return false;
            /*var uri = apiDesc.Route.RouteTemplate;
            var versionStr = Regex.Match(uri, @"(?<=^resources/)[a-zA-Z0-9]+(?=/)").Value;
            return versionStr == version;*/
        }
    }

    public class SwashbuckleVersionDefinition
    {
        public SwashbuckleVersionDefinition(string version, string title)
        {
            this.Version = version;
            this.Title = title;
        }

        public string Version { get; set; }
        public string Title { get; set; }
    }

    #region DocumentFilters

    /// <summary>
    /// Add swagger documentation for token. Need to generate this manually
    /// Ref: https://github.com/domaindrivendev/Swashbuckle/issues/332
    /// </summary>
    class AuthTokenOperation : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            swaggerDoc.paths.Add("/token", new PathItem
            {
                post = new Operation
                {
                    tags = new List<string> { "Token" },
                    summary = "Endpoint to get access token",
                    description = "Endpoint to get access token",
                    consumes = new List<string>
                    {
                        "application/x-www-form-urlencoded"
                    },
                    parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            type = "string",
                            name = "grant_type",
                            required = true,
                            description = "Put 'password' for login and 'refresh_token' for refresh token",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "username",
                            required = false,
                            description = "Required when grant_type=password",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "password",
                            required = false,
                            description = "Required when grant_type=password",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_id",
                            required = true,
                            description = "The client id",
                            @in = "formData"
                        },
                        new Parameter
                        {
                            type = "string",
                            name = "client_secret",
                            required = false,
                            description = "The client secret. Required for native apps, eg. mobile or desktop apps",
                            @in = "formData"
                        }
                    }
                }
            });
        }
    #endregion
}
}
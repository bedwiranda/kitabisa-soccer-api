﻿using Kitabisa.Api.Soccer.Versionings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Kitabisa.Api.Soccer.Core.Services.Players;
using Kitabisa.Api.Soccer.Models.Players;
using Kitabisa.Api.Soccer.Core.DTO.Players;
using Kitabisa.Api.Soccer.Core.Entities;
using Swashbuckle.Swagger.Annotations;
using System.Net;

namespace Kitabisa.Api.Soccer.Controllers
{
    [RoutePrefix("players")]
    public class PlayerController : ApiController
    {
        private readonly IPlayerCommandService _commandService;
        private readonly IPlayerQueryService _queryService;

        public PlayerController(
            IPlayerCommandService commandService, IPlayerQueryService queryService)
        {
            _commandService = commandService;
            _queryService = queryService;
        }


        #region COMMAND SERVICE
        /// <summary>
        ///     Create Player
        /// </summary>
        /// <remarks>Create Player</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(Player))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Player))]
        [HttpPost]
        public async Task<IHttpActionResult> AddPlayerAsync(
            [FromBody] PlayerCreateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new PlayerCreateDTO()
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                PhoneNumber = data.PhoneNumber,
                Email = data.Email
            };

            var result =
                await _commandService.CreatePlayerAsync(dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }


        /// <summary>
        ///     Update Player
        /// </summary>
        /// <remarks>Update Player</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("{playerId:guid}")]
        [ResponseType(typeof(Player))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Player))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdatePlayerAsync(Guid playerId,
            [FromBody] PlayerUpdateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new PlayerUpdateDTO()
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                PhoneNumber = data.PhoneNumber,
                Email = data.Email
            };

            var result =
                await _commandService.UpdatePlayerAsync(playerId, dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }

        #endregion



        #region QUERY SERVICE

        [VersionedRoute("")]
        [ResponseType(typeof(ICollection<Player>))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ICollection<Player>))]
        [HttpGet]
        public async Task<IHttpActionResult> GetPlayerAsync()
        {
            var result =
                await _queryService.GetPlayerAsync();

            return Ok(result);
        }

        [VersionedRoute("search")]
        [ResponseType(typeof(Player))]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ICollection<Player>))]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchPlayerAsync(
            [FromUri] string keyword = null
            )
        {
            var result =
                await _queryService.GetPlayerByNameAsync(keyword);

            return Ok(result);
        }

        #endregion
    }
}

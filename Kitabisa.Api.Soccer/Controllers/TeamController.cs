﻿using Kitabisa.Api.Soccer.Versionings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Models.Teams;
using Kitabisa.Api.Soccer.Core.DTO.Teams;
using Kitabisa.Api.Soccer.Core.Entities;

namespace Kitabisa.Api.Soccer.Controllers
{
    [RoutePrefix("teams")]
    public class TeamController : ApiController
    {
        private readonly ITeamCommandService _commandService;
        private readonly ITeamQueryService _queryService;

        public TeamController(
            ITeamCommandService commandService, ITeamQueryService queryService)
        {
            _commandService = commandService;
            _queryService = queryService;
        }


        /// <summary>
        ///     Create Team
        /// </summary>
        /// <remarks>Create Team</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(bool))]
        [HttpPost]
        public async Task<IHttpActionResult> AddTeamAsync(
            [FromBody] TeamCreateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new TeamCreateDTO()
            {
                Name = data.Name,
                Phone = data.Phone,
                Address = data.Address,
                Email = data.Email
            };

            var result =
                await _commandService.CreateTeamAsync(dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }


        /// <summary>
        ///     Update Team
        /// </summary>
        /// <remarks>Update Team</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("{teamId:guid}")]
        [ResponseType(typeof(bool))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTeamAsync(Guid teamId,
            [FromBody] TeamUpdateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new TeamUpdateDTO()
            {
                Name = data.Name,
                Phone = data.Phone,
                Address = data.Address,
                Email = data.Email
            };

            var result =
                await _commandService.UpdateTeamAsync(teamId, dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }

        #region QUERY SERVICE

        [VersionedRoute("")]
        [ResponseType(typeof(ICollection<Team>))]
        [HttpGet]
        public async Task<IHttpActionResult> GetTeamAsync()
        {
            var result =
                await _queryService.GetTeamAsync();

            return Ok(result);
        }

        [VersionedRoute("search")]
        [ResponseType(typeof(Team))]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchTeamAsync(
            [FromUri] string keyword = null)
        {
            var result =
                await _queryService.GetTeamByNameAsync(keyword);

            return Ok(result);
        }

        #endregion
    }
}

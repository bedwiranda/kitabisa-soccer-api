﻿using Kitabisa.Api.Soccer.Versionings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Kitabisa.Api.Soccer.Core.Services.Teams;
using Kitabisa.Api.Soccer.Models.Teams;
using Kitabisa.Api.Soccer.Core.DTO.Teams;
using Kitabisa.Api.Soccer.Core.Services.TeamPlayers;
using Kitabisa.Api.Soccer.Models.TeamPlayers;
using Kitabisa.Api.Soccer.Core.DTO.TeamPlayers;
using Kitabisa.Api.Soccer.Core.Entities;

namespace Kitabisa.Api.Soccer.Controllers
{
    [RoutePrefix("team-players")]
    public class TeamPlayerController : ApiController
    {
        private readonly ITeamPlayerCommandService _commandService;
        private readonly ITeamPlayerQueryService _queryService;

        public TeamPlayerController(
            ITeamPlayerCommandService commandService, ITeamPlayerQueryService queryService)
        {
            _commandService = commandService;
            _queryService = queryService;
        }


        /// <summary>
        ///     Create Team Player
        /// </summary>
        /// <remarks>Create Team</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(bool))]
        [HttpPost]
        public async Task<IHttpActionResult> AddTeamPlayerAsync(
            [FromBody] TeamPlayerCreateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new TeamPlayerCreateDTO()
            {
                TeamId = data.TeamId,
                PlayerId = data.PlayerId,
                BackNumber = data.BackNumber,
                Position = data.Position
            };

            var result =
                await _commandService.CreateTeamPlayerAsync(dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }


        /// <summary>
        ///     Update Team Player
        /// </summary>
        /// <remarks>Update Team</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>
        [VersionedRoute("")]
        [ResponseType(typeof(bool))]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateTeamAsync(
            [FromBody] TeamPlayerUpdateVM data)
        {
            //we use manual mapping here, actually we can use library like AutoMapper but i did not use it here due to need more configuration

            var dto = new TeamPlayerUpdateDTO()
            {
                TeamId = data.TeamId,
                PlayerId = data.PlayerId,
                BackNumber = data.BackNumber,
                Position = data.Position
            };

            var result =
                await _commandService.UpdateTeamPlayerAsync(dto);

            if (result.Succeded)
            {
                return Ok(result.Data);
            }

            return Ok(false);
        }


        #region QUERY SERVICE

        [VersionedRoute("{teamId:guid}")]
        [ResponseType(typeof(ICollection<Player>))]
        [HttpGet]
        public async Task<IHttpActionResult> GetTeamPlayerAsync(Guid teamId)
        {
            var result =
                await _queryService.GetTeamPlayerAsync(teamId);

            return Ok(result);
        }

        [VersionedRoute("{teamId:guid}/search")]
        [ResponseType(typeof(Player))]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchTeamPlayerAsync(Guid teamId,
            [FromUri] string keyword = null)
        {
            var result =
                await _queryService.GetTeamPlayerByNameAsync(teamId, keyword);

            return Ok(result);
        }

        #endregion

        
    }
}

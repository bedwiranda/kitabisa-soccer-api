﻿using Kitabisa.Api.Soccer.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kitabisa.Api.Soccer.Models.TeamPlayers
{
    public class TeamPlayerEntryVM
    {
        public Guid TeamId { get; set; }

        public Guid PlayerId { get; set; }

        public string BackNumber { get; set; }

        public PositionType Position { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kitabisa.Api.Soccer.Versionings
{
    public class VersionConst
    {
        private static int _apiLatestVersion;

        public const string ApiVersionHeaderName = "api-version";

        /// <summary>
        /// This property must be set by the api project on app start
        /// </summary>
        public static int ApiLatestVersion
        {
            get
            {
                return _apiLatestVersion;
            }
            set
            {
                _apiLatestVersion = value;
            }
        }
    }
}
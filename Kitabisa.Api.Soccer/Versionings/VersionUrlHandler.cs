﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Kitabisa.Api.Soccer.Versionings
{
    public class VersionUrlHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var uri = request.RequestUri.AbsoluteUri;
            var absolutePath = request.RequestUri.AbsolutePath;

            if ((absolutePath != "/") && absolutePath.StartsWith("/resources/", StringComparison.OrdinalIgnoreCase))
            {
                //convert version no in uri to query string
                var uriVersion = GetVersionFromUri(absolutePath);
                if (uriVersion != null)
                {
                    var query = HttpUtility.ParseQueryString(request.RequestUri.Query);
                    query["v"] = uriVersion.Value.ToString(CultureInfo.InvariantCulture);

                    //var rgx = new Regex(@"(?<=/)resources(?=/)");
                    var rgx = new Regex(@"(?<=/)resources/v\d+(?=/)");
                    var newUri = rgx.Replace(uri, "resources", 1);
                    var newUriUb = new UriBuilder(newUri) { Query = query.ToString() };
                    request.RequestUri = newUriUb.Uri;
                }
            }

            var response = await base.SendAsync(request, cancellationToken);
            return response;
        }

        private static int? GetVersionFromUri(string uri)
        {
            var versionStr = Regex.Match(uri, @"(?<=^/resources/v)\d+(?=/)").Value;

            int version;
            if (!string.IsNullOrEmpty(versionStr) && Int32.TryParse(versionStr, out version))
            {
                return version;
            }

            return null;
        }

        /*protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var uri = request.RequestUri.AbsoluteUri;
            var absolutePath = request.RequestUri.AbsolutePath;
            //string host = request.RequestUri.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

            if ((absolutePath != "/") && absolutePath.StartsWith("/resources/", StringComparison.OrdinalIgnoreCase))
            {
                //add version no to new uri if does not exists in current url
                if (GetVersionFromUri(absolutePath) == null)
                {
                    var version = GetVersionFromHeader(request) ?? ApiGlobalConst.ApiLatestVersion;

                    var rgx = new Regex(@"(?<=/)resources(?=/)");
                    var newUri = rgx.Replace(uri, "resources/v" + version.ToString(CultureInfo.InvariantCulture), 1);
                    //var newUri = Regex.Replace(uri, @"(?<=/)resources(?=/)", "resources/v" + version.ToString(), 1);
                    //var newUri = string.Format("{0}/v{1}{2}", host, version.ToString(), absolutePath);
                    request.RequestUri = new UriBuilder(newUri).Uri;
                }
            }

            var response = await base.SendAsync(request, cancellationToken);
            return response;
        }

        private static int? GetVersionFromUri(string uri)
        {
            var versionStr = Regex.Match(uri, @"(?<=^/resources/v)\d+(?=/)").Value;
            //var versionStr = Regex.Match(uri, @"(?<=^/[v])\d+(?=/)").Value;

            int version;
            if (Int32.TryParse(versionStr, out version))
            {
                return version;
            }

            return null;
        }

        private static int? GetVersionFromHeader(HttpRequestMessage request)
        {
            string versionStr;
            IEnumerable<string> headerValues;

            if (request.Headers.TryGetValues(ApiGlobalConst.ApiVersionHeaderName, out headerValues))
            {
                var headerValuesEnumerable = headerValues.ToList();
                if (headerValuesEnumerable.Count() == 1)
                {
                    versionStr = headerValuesEnumerable.First();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

            int version;
            if (versionStr != null && Int32.TryParse(versionStr, out version))
            {
                return version;
            }

            return null;
        }*/
    }
}
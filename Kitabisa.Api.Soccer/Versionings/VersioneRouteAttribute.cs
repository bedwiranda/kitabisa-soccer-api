﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Kitabisa.Api.Soccer.Versionings
{
        /// <summary>
        /// Custom attribute for versioning
        /// Enter 0 in allowedVersion to use latest version
        /// Ref: http://robertgaut.com/Blog/2007/Four-Ways-to-Version-Your-MVC-Web-API
        /// Ref: https://app.pluralsight.com/library/courses/building-securing-restful-api-aspdotnet/table-of-contents
        /// Ref: https://app.pluralsight.com/library/courses/implementing-restful-aspdotnet-web-api/table-of-contents
        /// </summary>
        public class VersionedRouteAttribute : RouteFactoryAttribute
        {
            public VersionedRouteAttribute(string template)
                : base(template)
            {
                AllowedVersion = 1;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="template"></param>
            /// <param name="allowedVersion">Enter equal or less than 0 in allowedVersion to use latest version</param>
            public VersionedRouteAttribute(string template, int allowedVersion)
                : base(template)
            {
                AllowedVersion = (allowedVersion > 0) ? allowedVersion : 1;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="template"></param>
            /// <param name="allowedVersion">Enter equal or less than 0 in allowedVersion to use latest version</param>
            /// <param name="minimumVersion"></param>
            public VersionedRouteAttribute(string template, int allowedVersion, int minimumVersion)
                : base(template)
            {
                AllowedVersion = (allowedVersion > 0) ? allowedVersion : 1;
                MinimumVersion = minimumVersion;
            }

            public int AllowedVersion
            {
                get;
                private set;
            }

            public int? MinimumVersion
            {
                get;
                private set;
            }
        }
}
Question 1: 

Algorithm:

Assumption:
- The data is only integer (not double), we can use double as well, but as the requirement does not state anything, so I just use integer

Here because the requirements is only for addition and substraction, so the algorithm is:
- We add + sign in the beginning of string if the start element is not - and +
- Looping from the end of the string
- Find the operator + or -
- Set the lastOperatorIndex
- If we find + or -, then we will do the calculation
- We get the number by using substring and then parse the substring to integer

For this question, I added the class into the Kitabisa.Api.Soccer.Tests so I don’t need to create new project.
These 2 classes will also I separate in case we need to run this in an online compiler.

For this question, I created 2 classes:

- AnswerClass
This class consist of the method to calculate the data from string:

- AnswerUnitTest
This class consists of unit tests for question2
There are some scenarios I used